;;; functional.scm ---

;; Copyright (C) 2014  <Dmitry Bogatov <KAction@gnu.org>>

;; Author:  <Dmitry Bogatov <KAction@gnu.org>>

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 3
;; of the License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;; This module defines functions to operate on syntax objects similar
;; to common function operations saving referential transparency.

;;; Code:
(define-module (syntax functional))

(use-modules (srfi srfi-1))
(use-modules (ice-9 match))
(use-modules (ice-9 control))

(define-syntax sealed (const #'#f))
(eval-when (compile)
  (let ((thales-module (resolve-module '(thales seal) #:ensure #f)))
    (if thales-module
	(module-define! (current-module) 'sealed
          (module-ref thales-module 'sealed))
	(format #t "~a\n~a\n"
		"Module (thales seal) is not available."
		"GNU Thales based testing is disabled."))))

(sealed s-car
  (#'(foo bar) *#* foo)
  (#'(foo) *#* foo)
  (#'() *!* syntax-error))

(define-public (s-car x)
  (syntax-case x ()
    ((car . cdr) #'car)))

(sealed s-cdr
  (#'(foo bar) *#* (bar))
  (#'(foo) *#* ()))

(define-public (s-cdr x)
  (syntax-case x ()
    ((car . cdr) #'cdr)))

(sealed s-cons
  (#'x #'y *#* (x . y))
  (#'x #'y #'z *!* wrong-number-of-args))

(define-public (s-cons x y)
  #`(#,x . #,y))

(define-public (s-xcons x y)
  (s-cons y x))

(sealed s-null?
  (#'(foo) *** #f)
  (#'() *** #t))

(define (s-null? x)
  (equal? x #'()))

(define-public (s-fold fn init list1 . more)
  ;; One more line to make explicit in signature
  ;; that at least one list must be present.
  (let ((lists (cons list1 more)))
    (if (any s-null? lists)
	init
	(let* ((vals (map s-car lists))
	       (tails (map s-cdr lists))
	       (args `(,@vals ,init)))
	  (apply s-fold fn (apply fn args) tails)))))

(define-public (s-fold-right fn init list1 . more)
  ;; One more line to make explicit in signature
  ;; that at least one list must be present.
  (let ((lists (cons list1 more)))
    (if (any s-null? lists)
	init
	(let ((vals (map s-car lists))
	      (tails (map s-cdr lists)))
	  (apply fn `(,@vals ,(apply s-fold-right fn init tails)))))))

(sealed s-list
  (*#* ())
  (#'x *#* (x))
  (#'foo #'bar *#* (foo bar)))

(define-public (s-list . args)
  (s-fold-right s-cons #'() args))

(sealed s-reverse
	(#'(foo bar) *#* (bar foo)))

(define-public (s-reverse x)
  (s-fold s-cons #'() x))

(sealed s-map-in-order
  (s-car #'((foo bar) (qq quaz)) *#* (foo qq)))

(define-public (s-map-in-order f list1 . more)
  (s-reverse (apply s-fold
		    (match-lambda*
		     ((args ... acc) (s-cons (apply f args) acc)))
		    #'() list1 more)))

(define-public s-map s-map-in-order)

(sealed s-filter
  (identifier? #'(foo (bar oo)) *#* (foo)))

(define-public (s-filter p list)
  (syntax-case list ()
    (() #'())
    ((car . cdr)
     (let ((filtered-tail (s-filter p #'cdr)))
       (if (p #'car)
	   (s-cons #'car filtered-tail)
	   filtered-tail)))))

(sealed s-begin
  (#f #'foo #'bar *#* (begin foo bar)))

(define-public (s-begin . args)
  (define (significant? x)
    (and x (not (eq? x *unspecified*))))
  (s-cons #'begin (s-filter significant? args)))


;; FIXME: Write seals.
(define-public (s->if-datum pred? list)
  (s-fold-right
   (lambda (x acc)
     (define datum (syntax->datum x))
     (xcons acc (if (pred? datum) datum x)))
   '()
   list))
